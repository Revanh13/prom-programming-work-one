import type { VocabularyItem } from "$lib/models";
import { GetWords } from "$lib/server";
import type { PageServerLoad } from "./$types";

export const load = (async () => {
	let words = await GetWords();

	if (words == undefined) return { words: new Array<VocabularyItem>() };

	return { words };
}) satisfies PageServerLoad;
