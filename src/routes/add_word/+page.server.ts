import { fail, redirect } from "@sveltejs/kit";
import type { Actions, PageServerLoad } from "./$types";
import type { VocabularyItem } from "$lib/models";
import { AddVocabularyItem } from "$lib/server";

export const load = (async () => {
	return {};
}) satisfies PageServerLoad;

export const actions = {
	default: async ({ cookies, request }) => {
		const data = await request.formData();

		const original_word = data.get("original_word");
		const translated_word = data.get("translated_word");

		const item: VocabularyItem = {
			original_word: original_word!.toString(),
			translated_word: translated_word!.toString(),
		};

		const res = await AddVocabularyItem(item);

		if (res == false)
			return { success: false, message: "Ошибка при добавлении перевода в базу!" };

		throw redirect(301, "/words_list");
	},
} satisfies Actions;
