export type VocabularyItem = {
	original_word: string;
	translated_word: string;
};
