import type { VocabularyItem } from "$lib/models";
import prisma from "./prisma";

export async function GetWords(): Promise<VocabularyItem[] | undefined> {
	try {
		return await prisma.vocabulary.findMany({});
	} catch (error) {
		console.log(error);
		return undefined;
	}
}

export async function AddVocabularyItem(item: VocabularyItem): Promise<boolean> {
	try {
		const form = await prisma.vocabulary.create({
			data: { original_word: item.original_word, translated_word: item.translated_word },
		});
		return true;
	} catch (error) {
		console.log(error);
		return false;
	}
}
