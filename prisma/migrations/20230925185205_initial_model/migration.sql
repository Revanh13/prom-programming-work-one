-- CreateTable
CREATE TABLE "Vocabulary" (
    "id" INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
    "original_word" TEXT NOT NULL,
    "translated_word" TEXT NOT NULL
);
